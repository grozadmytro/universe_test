//
//  ViewController.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
  
  let provider = ImageProvider()
  let saver = ImageSaver()
  
  let disposeBag = DisposeBag()
  let processingScheduler = ConcurrentDispatchQueueScheduler(qos: .userInitiated)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let images = (0..<1000).map { index in provider.getImage(index: index).subscribeOn(processingScheduler) }
    
    Observable
      .zip(images)
      .flatMap { images in
        return Observable.zip(
          images
            .map { image in
              return self.saver.saveImage(image).subscribeOn(self.processingScheduler)
          }
        )
    }
    .subscribe(onNext: { results in
      let saveResult = results.reduce(into: (0, 0)) { (counter, result) in
        switch result {
        case .failure:
          counter.0 += 1
        case .success:
          counter.1 += 1
        }
      }
      
      print("Operation complete. Failed - \(saveResult.0); Saved - \(saveResult.1)")
    })
      .disposed(by: disposeBag)
  }
}
